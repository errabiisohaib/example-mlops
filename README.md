# À propos
Ce projet consiste à mettre en oeuvre les techniques de MLOps 
apprises pendant les séances de cours pour mettre en place de l'intégration continue,
une documentation de l'API, et un benchmark des performances d'un modèle de machine learning.

# Sujet
Le sujet du projet est disponible ici : https://tinyurl.com/mlops-paper

# Contributeurs

- Sohaib Errabii ([serrabii@enseirb-matmeca.fr](mailto:serrabii@enseirb-matmeca.fr))
- Jalal Izekki ([jizekki@enseirb-matmeca.fr](mailto:jizekki@enseirb-matmeca.fr))