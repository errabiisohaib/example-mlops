# syntax=docker/dockerfile:1

FROM python:3.7-buster

RUN pip install poetry==1.1.12

WORKDIR /app
COPY poetry.lock pyproject.toml ./

RUN poetry config virtualenvs.create false \
    && poetry install

COPY . .

CMD ["python",  "main.py"]
